import numpy as np

def config(data_name, n_channel):
    if data_name == 'mnist':
        labels = [[i] for i in xrange(10)]
        n_iter = 1
        dimX = 28**2
        shape_high = (28, 28)
        ll = 'bernoulli'
    if data_name == 'notmnist':
        labels = [[i] for i in xrange(10)]
        n_iter = 400
        dimX = 28**2
        shape_high = (28, 28)
        ll = 'bernoulli'
    if data_name == 'permuted_mnist':
        n_iter = 10  #number of epochs that you want
        dimX = 28**2
        shape_high = (28, 28)
        ll = 'bernoulli'
        labels = [[i] for i in xrange(10)]

        
    return labels, n_iter, dimX, shape_high, ll

